package br.com.alura.screenmatch.calculos;

import br.com.alura.screenmatch.modelos.Titulo;

public class CalculadoraDeTempo {

    private int tempoTotal;

    public int getTempoTotal() {
        return tempoTotal;
    }

    public void setTempoTotal(int tempoTotal) {
        this.tempoTotal = tempoTotal;
    }

    //Exemplo de polimorfismo. Diversas formas. Não sei quem vai chamar aqui, se será filme, se será série ou outros que
    // possam surgir.
    public void inclui(Titulo titulo){
        System.out.println("Adicionando duração em minutos de: " + titulo);
        this.tempoTotal += titulo.getDuracaoEmMinutos();
    }
    // O método não chama o getDuracaoEmMinutos de titulo, mas ele chama
    // o que está na subclasse específica que está invocando este método.
}
